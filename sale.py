# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.transaction import Transaction
from trytond.pyson import Eval

__all__ = ['Sale']


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def get_stock_products(self, date, product_ids=[]):
        pool = Pool()
        Product = pool.get('product.product')

        transaction = Transaction()
        if not product_ids:
            product_ids = [line.product.id for line in self.lines
                if line.product]
        with transaction.set_context(
                    stock_skip_warehouse=True,
                    stock_date_end=date):
            pbl = Product.products_by_location(
                    [self.warehouse.id],
                    with_childs=True,
                    grouping_filter=(product_ids,))
        delta = {}
        for key, qty in list(pbl.items()):
            _, product_id = key
            delta[product_id] = qty
        return delta

    @classmethod
    def process(cls, sales):
        for sale in sales:
            sale.set_supply_production()
        super(Sale, cls).process(sales)

    def set_supply_production(self):
        Date = Pool().get('ir.date')

        if self.state != 'confirmed':
            return
        date = self._get_supply_stock_date()
        products_stock = self.get_stock_products(date)
        for line in self.lines:
            if not line.supply_production:
                continue
            if line.product and line.product.id in products_stock:
                remaining_stock = (products_stock[line.product.id] -
                    line._move_remaining_quantity)
            else:
                remaining_stock = -line.quantity
            if getattr(self, 'do_shipment', False) and self.shipments:
                # if shipment is already created on future, must be discarded
                if date == self.shipments[0].effective_date and \
                       date > Date.today():
                    remaining_stock += line._get_shipped_quantity('out')

            if remaining_stock > 0:
                remaining_stock = 0
            line.remaining_stock = remaining_stock
            line.supply_production = line.remaining_stock < 0
            line.save()

    def _get_supply_stock_date(self):
        return self.sale_date


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    remaining_stock = fields.Float('Remaining stock', readonly=True,
        digits=(16, Eval('unit_digits', 2)),
        depends=['unit_digits'])

    def create_productions(self):
        pool = Pool()
        Production = pool.get('production')

        if not self._must_create_production():
            return

        # find if inputs of boms have iterate while boms
        input_products = {b.bom: b.bom.inputs[0].product
            for b in self.product.boms}
        product_qty = self.sale.get_stock_products(
            self.sale._get_supply_stock_date(),
            product_ids=[p.id for p in list(input_products.values())])

        bom_qty = {k: 0 for k, _ in input_products.items()}
        assigned_qty = 0

        for bom in self.product.boms:
            bom = bom.bom
            product_id = bom.inputs[0].product.id
            if product_id not in product_qty or product_qty[product_id] <= 0:
                continue
            qty = product_qty[product_id]

            # consider bom input qty
            bom_qty[bom] = min(qty,
                abs(self.remaining_stock) - assigned_qty)
            assigned_qty += bom_qty[bom]

            if assigned_qty >= abs(self.remaining_stock):
                break

        production_values = []
        for bom, qty in bom_qty.items():
            if qty > 0:
                production_values.append({
                    'product': self.product,
                    'uom': self.unit,
                    'quantity': qty,
                    'bom': bom
                    })

        productions = []
        for values in production_values:
            production = self.get_production(values)

            if production:
                if hasattr(production, 'bom') and production.bom:
                    production.inputs = []
                    production.outputs = []
                    production.explode_bom()

                production.save()
                productions.append(production)

        if not productions:
            return

        Production.wait(productions)
        Production.assign(productions)
        Production.run(productions)
        Production.done(productions)

    def get_production(self, values):
        res = super(SaleLine, self).get_production(values)
        res.effective_date = self.sale._get_supply_stock_date()
        res.effective_start_date = self.sale._get_supply_stock_date()
        return res

    def _must_create_production(self):
        if (self.type != 'line' or
                not self.product or
                not self.product.template.producible or
                self.quantity <= 0 or
                len(self.productions) > 0):
            return False
        if self.sale.state == 'processing' and self.moves:
            # TODO: manage reprocessing
            return False
        if not self.remaining_stock:
            return False
        if not self.product.producible or not self.product.boms:
            return False
        return True
