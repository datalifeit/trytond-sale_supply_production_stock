datalife_sale_supply_production_stock
=====================================

The sale_supply_production_stock module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_supply_production_stock/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_supply_production_stock)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
